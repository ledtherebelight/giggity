require 'test_helper'

class WebhooksControllerTest < ActionController::TestCase
  test "should get create" do
    get :create
    assert_response :success
  end

  test "should get destroy_all" do
    get :destroy_all
    assert_response :success
  end

  test "should get handle" do
    get :handle
    assert_response :success
  end

  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get verify" do
    get :verify
    assert_response :success
  end

end
