require 'test_helper'

class TransactionsControllerTest < ActionController::TestCase
  test "should post create" do
    offer = Offer.create(
      permalink: 'test_offer',
      price:     100
    )

    email = 'pete@example.com'
    token = 'tok_test'

    post :create, email: email, stripeToken: token

    assert_not_nil assigns(:sale)
    assert_not_nil assigns(:sale).stripe_id
    assert_equal offer.id, assigns(:sale).offer_id
    assert_equal email, assigns(:sale).email
  end
end