Rails.application.routes.draw do
  #resources :sales # enable later when admin is set up

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  root 'pages#home'

  devise_for 	:users,
  				path: '',
  				path_names: {
  				  sign_in: 'login',
  				  sign_out: 'logout',
  				  edit: 'profile'
  				},
  				controllers: {
  				  omniauth_callbacks: 'omniauth_callbacks',
  				  registrations: 'registrations'

  				}

  resources :phone_numbers, only: [:new, :create]
  post 'phone_numbers/verify' => 'phone_numbers#verify'
  post 'provide_services' => 'pages#provide_services'
  #post 'about_me' => 'pages#about_me' not needed

  resources :users, only: [:show, :destroy] do
    get 'delete-account' => 'users#delete_account'
    resources :reviews, only: [:destroy]
    # For Braintree merchant creation
    #resources :merchants, only: [:new, :create]
  end



  resources :photos

  resources :gig_categories do
    resources :gig_sub_categories do
      resources :gigs
    end
  end

  resources :gigs, only: [:show, :edit, :update, :destroy, :index] do
    #resources :reviews, only: [:create, :destroy]
    resources :offers, only: [:create, :index, :edit, :update, :destroy] do
      resources :reviews, only: [:new, :create]
    # member do
    #   post :cancel
    # end
    end
  end

  resources :conversations, only: [:index, :create] do
    resources :messages, only: [:index, :create]
  end

  resources :offers do
    # braintree routes
    # resources :transactions, only: [:new, :create]
    member do
      post :inactivate
      post :accept
      post :reject
      post :sign_off
    end
  end

  #resources :after_signup


  # stripe routes
  # get  '/buy/:offer_id', to: 'transactions#new',      as: :show_buy
  # post '/buy/:offer_id', to: 'transactions#create',   as: :buy
  # get  '/pickup/:guid',   to: 'transactions#pickup',   as: :pickup
  # get '/auth/stripe_connect/callback', to: 'stripe_connect#create'
  # mount StripeEvent::Engine => '/stripe-events'

  # braintree webhooks
  # match 'webhooks/process' => 'webhooks#handle',
  #   :as => :process_webhook, :via => :post
  # match 'webhooks/process' => 'webhooks#verify',
  #   :as => :verify_webhook, :via => :get



  # mount Sidekiq::Web => '/sidekiq'

  # post 'bank_details' => 'bank_details#create'
  # get 'bank_details' => 'bank_details#show'


  # get '/preload' => 'offers#preload'
  # get '/preview' => 'offers#preview'

  # get '/your_trips' => 'offers#your_trips'
  get '/dashboard' => 'offers#dashboard'
  get '/your-purchases' => 'offers#your_purchases'

  # post '/notify' => 'offers#notify'
  # post '/your_trips' => 'offers#your_trips'

  get 'how_it_works_layout_test' => 'pages#how_it_works_layout_test'
  #get 'pages/gigs' => 'pages#gigs'
  #get '/lawncare' => 'pages#lawncare'
  get '/howitworks' => 'pages#howitworks'
  #get '/housework' => 'pages#housework'
  #get '/other' => 'pages#other'
  get '/search' => 'pages#search'
  get '/privacy-policy' => 'pages#privacy_policy'
  get '/terms' => 'pages#terms'


  # Twilio
  post 'tokens' => "tokens#create"
end
