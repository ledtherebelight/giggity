class Poster
  include Sidekiq::Worker

  attr_accessor :gig_category, :gig_sub_category

  def perform
    Gig.create(
      gig_category: get_gig_category,
      gig_sub_category: get_gig_sub_category,
      name: get_name,
      summary: get_summary,
      user_id: get_user_id,
      active: true,
      zip_code: get_zip_code,
      address: get_address,
    )
    # Create listing
    # Params:
      # name
      # summary
      # user_id
      # active: true
      # zip_code
      # gig_category
      # gig_sub_category
      # address
      # zip_code

  end

  def get_gig_category
    @gig_category = GigCategory.order("RANDOM()").first
  end

  def get_gig_sub_category
    @gig_sub_category = @gig_category.gig_sub_categories.order("RANDOM()").first
  end

  def get_user_id
    @user_id = User.where(fake: true).sample.id
  end

  def get_name
    # Switch based on category/sub-category

    case @gig_category.name
    when "A"
    end
  end
end

Sidekiq::Cron::Job.create(name: 'Hard worker - every 5min', cron: '*/5 * * * *', class: 'HardWorker')

# gig = Gig.create(name: "Photographer for wedding", summary: "Looking for someone to take pictures at our wedding in Murrysville and our reception near the Point. Please send a link to online portfolio, if available, or send a minimum of five photos via a zip file. Previous experience with a wedding a plus.", user_id: User.where(fake: true).sample.id, active: true, zip_code: pgh_zips.sample, gig_category: GigCategory.find_by(name: "Random"), gig_sub_category: GigSubCategory.find_by(name: "Misc"), address: "murrysville pa", created_at: (Time.now - 5.hours))