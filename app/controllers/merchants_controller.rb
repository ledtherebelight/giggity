class MerchantsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_user_phone!

  def new
    @user = current_user

  end

  # need to add updating

  def create
    @user = current_user
    merchant_account_params = {
      :individual => {
        :first_name => @user.first_name,
        :last_name => @user.last_name,
        :email => @user.email,
        :date_of_birth => @user.birthday,
        :address => {
          :street_address => params[:address],
          :locality => params[:city],
          :region => params[:state],
          :postal_code => params[:zip_code]
        }
      },
      :funding => {
        :descriptor => @user.email,
        :destination => Braintree::MerchantAccount::FundingDestination::Email,
        :email => @user.email,
      },
      :tos_accepted => true,
      :master_merchant_account_id => ENV["BRAINTREE_MASTER_MERCHANT_ID"],
      :id => "#{current_user.first_name}_#{current_user.last_name}_#{SecureRandom.random_number(1_000_000).to_s(36)}"
    }
    @result = Braintree::MerchantAccount.create(merchant_account_params)

    if @result.success?
      current_user.update(merchant_account_approved: false)
      current_user.update(merchant_id: @result.merchant_account.id)
      redirect_to dashboard_path, notice: "Payment info added. We are confirming your information with Venmo."
    else
      flash[:alert] = "Something went wrong"
      render :new
    end
  end

end
