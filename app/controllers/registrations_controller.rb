class RegistrationsController < Devise::RegistrationsController
	#skip_before_filter :verify_authenticity_token, :only => :update

	protected
		# def after_update_path_for(resource)
		# 	edit_user_registration_path(resource)
		# end

		def after_sign_up_path_for(resource)
			new_phone_number_path
		end

		def update_resource(resource, params)
			resource.update_without_password(params)
		end
end