class SalesController < InheritedResources::Base

  before_action :authenticate_user!
  before_action :authenticate_user_phone!
  #before_action :authenticate_admin_user!

  def show
    @sale = Sale.find(params[:id])
  end

  private

    def sale_params
      params.require(:sale).permit(:email, :guid, :offer_id, :stripe_id)
    end
end

