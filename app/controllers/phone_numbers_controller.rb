class PhoneNumbersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user!

  def new
    if @user.phone_number && @user.phone_number.verified?
      redirect_to user_path(@user), notice: "Your phone number is already verified"
    elsif @user.phone_number
      @phone_number = @user.phone_number
    else
      @phone_number = @user.build_phone_number
    end
  end

  def create
    formatted_phone = PhonyRails.normalize_number(params[:phone_number][:phone_number], country_code: 'US')
    @phone_number = PhoneNumber.find_or_create_by(phone_number: formatted_phone)
    puts "phone number 1" + @phone_number.inspect
	  if !@phone_number.verified
	    puts "phone number 2" + @phone_number.inspect
      @phone_number.generate_pin
      @phone_number.send_pin
      # Reset attempts with new pin generation
      @phone_number.attempts = 0
      puts "attempts pre save: " + @phone_number.attempts.to_s
      @phone_number.save
      puts "attempts post save: " + @phone_number.attempts.to_s
    end

    respond_to do |format|
      format.js # render app/views/phone_number/create.js.erb
    end
  end

  def verify
    @phone_number = PhoneNumber.find_by(phone_number: params[:hidden_phone_number])
    @phone_number.verify(params[:pin], @user)
    respond_to do |format|
      format.js # render app/views/phone_number/verify.js.erb
    end
  end

  private
    def set_user!
      @user = current_user
    end
end
