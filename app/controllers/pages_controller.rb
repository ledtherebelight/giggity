class PagesController < ApplicationController
  before_action :authenticate_user!, only: [:provide_services, :delete_account]

  def home
  	@gigs = Gig.limit(3)
  	@gig_categories = GigCategory.all
  end

  def howitworks
    render layout: "how_it_works_layout"
  end

  def how_it_works_layout_test
    render layout: "how_it_works_layout_test"
  end

  def gigs
  end

  def lawncare
  end

  def housework
  end

  def other
  end

  def privacy_policy
  end

  def provide_services
    respond_to do |format|
      format.js
    end
  end

  # def about_me
  #   respond_to do |format|
  #     format.js
  #   end
  # end

  def search

    #########################################################
    # Location / Coords
    #########################################################
    # @location - used in URL params and Location Search Box
    # @coords - used for Google Map position
    #########################################################

    # Order of priority for geolocation
    # 1 - search params
    if !params[:location].blank?
      puts "inside if statement"
      @has_params_location = true
      @location = params[:location]
      @coords = Geocoder.coordinates(@location)
    # 2 - user sharing location
    # elsif # have user location
    #   @location
    # handled in javascript
    # 3 - latitude and longitude from user model
    elsif current_user && current_user.latitude && current_user.longitude && current_user.city
      puts "inside elsif statement"
      @location = current_user.city
      @coords = [current_user.latitude, current_user.longitude]
    # 4 - default location otherwise
    else
      puts "inside else statement"
      @location = "Palo Alto California"
      @coords = Geocoder.coordinates(@location)
    end
    puts "location: " + @location.inspect
    puts "coords: " + @coords.inspect

    @location = @coords = [40.7127,-74.0059] unless @coords





    # def lat_lng
    #   @lat_lng ||= session[:lat_lng] ||= get_geolocation_data_the_hard_way
    # end

    #puts "session: " + session[:lat_lng].inspect

  	if params[:search].present? && params[:search].strip != ""
  		session[:loc_search] = params[:search]
  	end

  	#arrResult = Array.new

  	if session[:loc_search] && session[:loc_search] != ""
  		@gigs_address = Gig.where(active: true).near(session[:loc_search], 25, order: 'distance')
  	else
  		@gigs_address = Gig.where(active: true).all
  	end

  	#puts "request.location: " + request.location.inspect

    # if params[:location].present?
    #   @location = params[:location]
    # # elsif request.location
    # #   @location = request.location.city
    # end
  # 	if Geocoder.coordinates(@location)
  # 	  @coords = Geocoder.coordinates(@location)
  # 	else
  # 	  @coords = [40.7127,-74.0059]
  # 	end

    if params[:within].present? && (params[:within].to_i > 0)
      @search = Gig.near(@location,params[:within]).search(params[:q])
    else
      @search = Gig.search(params[:q])
    end
  	#@search = @gigs_address.ransack(params[:q])
  	@gigs = @search.result.where(active: true).order("created_at desc")
  	@arrGigs = @gigs.paginate(page: params[:page], per_page: 20)

    render layout: "search_layout"

  end
end


  # 	puts "gig address: " + @gigs_address.inspect
  # 	puts "arrGigs: " + @arrGigs.inspect
  # 	puts "search: " + @search.inspect
  # 	puts "location: " + @location.inspect
  # 	puts "coords: " + @coords.inspect



  # 	if (params[:start_date] && params[:end_date] && !params[:start_date].empty? & !params[:end_date].empty?)

  # 		# start_date = Date.parse(params[:start_date])
  # 		# end_date = Date.parse(params[:end_date])

  # 		# @gigs.each do |gig|

  # 		# 	not_available = gig.offers.where(
  # 		# 			"(? <= start_date AND start_date <= ?)
  # 		# 			OR (? <= end_date AND end_date <= ?)
  # 		# 			OR (start_date < ? AND ? < end_date)",
  # 		# 			start_date, end_date,
  # 		# 			start_date, end_date,
  # 		# 			start_date, end_date
  # 		# 		).limit(1)

  # 		# 	if not_available.length > 0
  # 		# 		@arrGigs.delete(gig)
  # 		# 	end

  # 		end

  # 	end