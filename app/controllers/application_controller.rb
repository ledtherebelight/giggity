class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :get_unread_count, if: :user_signed_in?

  def authenticate_admin_user!
    raise SecurityError unless current_user.try(:admin?)

    rescue_from SecurityError do |exception|
      redirect_to root_url
    end
  end

  def authenticate_user_phone!
    unless current_user and current_user.phone_confirmation
      flash[:danger] = "Please verify your account before doing that."
  		redirect_to new_phone_number_path
    end
  end

  def send_message_from_admin(user_id, message_content)
		if Conversation.between(User.find_by(email: "admin@giggity.rocks").id, user_id).present?
			conversation = Conversation.between(User.find_by(email: "admin@giggity.rocks").id, user_id).first
		else
			conversation_params = { sender_id: User.find_by(email: "admin@giggity.rocks").id, recipient_id: user_id }
		  conversation = Conversation.create(conversation_params)
		end
		message_params = { content: message_content, user_id: User.find_by(email: "admin@giggity.rocks").id }
		conversation.messages.create(message_params)
  end

# 	def check_merchant_account_status(user)
# 		if user.merchant_id == nil
# 			send_message_from_admin(user.id, "Thanks for submitting an offer! Now make sure we can pay you by visiting your Dashboard.")
# 		elsif user.merchant_account_approved == false
# 			send_message_from_admin(user.id, "Your offer went through, but we're still waiting to get confirmation on your payment details. We'll let you know when we hear back.")
# 		end
# 	end

  protected

    def get_unread_count
      unread_count = 0
      conversations = Conversation.involving(current_user)
      conversations.each do |conversation|
        if conversation.messages.any?
          if conversation.messages.last.unread?(current_user)
            unread_count += 1
          end
        end
      end
      @unread_count = unread_count
    end


  	def configure_permitted_parameters
  		devise_parameter_sanitizer.for(:sign_up) << :first_name << :last_name << :full_name << :email << :phone << :address << :zip_code << :gender << :birthday << :tos_accepted << :fake
  		devise_parameter_sanitizer.for(:account_update) << :first_name << :last_name << :phone << :birthday << :description << :password << :gender << :city << :state << :zip_code << :age << :slug << :avatar << :phone_confirmation << :tos_accepted
  	end
end
