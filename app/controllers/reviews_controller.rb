class ReviewsController < ApplicationController
	before_action :authenticate_user!
	before_action :authenticate_user_phone!

	def new
		@gig = Gig.friendly.find(params[:gig_id])
		@offer = Offer.friendly.find(params[:offer_id])
		@review = current_user.reviews_made.new
	end

	def create
		# if current_user && !Review.find_by(reviewer: current_user, reviewed: params[:review][:reviewed_id])
		# 	@review = current_user.reviews_made.create(review_params)
		# end
		# session[:return_to] ||= request.referer
		# redirect_to session.delete(:return_to)

		@gig = Gig.friendly.find(params[:gig_id])
		@offer = Offer.friendly.find(params[:offer_id])
		@other = current_user == @offer.lister ? @offer.bidder : @offer.lister

		@review = current_user.reviews_made.build(review_params)

		@review.reviewed_id = @other.id
		@review.reviewer_id = current_user.id
		@review.offer_id = @offer.id

		@review.is_for_lister = @other == @offer.lister ? true : false

		if @review.save
			flash[:notice] = "Review submitted. Thanks!"
			redirect_to dashboard_path
		else
			flash[:warning] = "Something went wrong, please try again"
			render :new
		end
	end

	def destroy
		@review = Review.find(params[:id])
		if current_user && (@review.reviewer == current_user)
			@review.destroy
		end

		session[:return_to] ||= request.referer
		redirect_to session.delete(:return_to)
	end

	private
		def review_params
			params.require(:review).permit(:comment, :star)
		end
end