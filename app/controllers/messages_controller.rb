class MessagesController < ApplicationController
	before_action :authenticate_user!
	before_action :authenticate_user_phone!
	before_action :set_conversation
	before_action :read_messages, only: [:index]

	def index
		if current_user == @conversation.sender || current_user == @conversation.recipient
			@other = current_user == @conversation.sender ? @conversation.recipient : @conversation.sender
			@messages = @conversation.messages.order("created_at ASC")
			# .last(3)
			#@messages_all = @conversation.messages.order("created_at ASC")

			@messages.each do |message|
				message.mark_as_read! for: current_user
			end
		else
			redirect_to conversations_path, alert: "You don't have permission to view this."
		end
	end

	def create
		@message = @conversation.messages.new(message_params)
		@messages = @conversation.messages.order("created_at DESC")
		other = current_user == @conversation.sender ? @conversation.recipient : @conversation.sender

		if @message.save
			# flash[:notice] = "Message sent"
			@message.mark_as_read! for: current_user

			# @message.mark_as_read! for: User.where.not(id: other.id)
			respond_to do |format|
				format.js
			end
		end
	end

	private
		def read_messages
			set_conversation
			@conversation.messages.last do |msg|
				msg.mark_as_read! for: current_user
			end
		end

		def set_conversation
			@conversation = Conversation.find(params[:conversation_id])
		end

		def message_params
			params.require(:message).permit(:content, :user_id)
		end
end