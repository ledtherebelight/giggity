class PhotosController < ApplicationController

	def destroy
		@photo = Photo.find(params[:id])
		gig = @photo.gig

		@photo.destroy
		@photos = Photo.where(gig_id: gig.id)

		respond_to :js
	end
end