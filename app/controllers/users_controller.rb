class UsersController < ApplicationController
	def show
		@user = User.friendly.find(params[:id])
		if request.path != user_path(@user)
			redirect_to @user, status: :moved_permanently
		end
		@reviews_received = @user.reviews_received.paginate(page: params[:page], per_page: 5)

		# @canReview
		# 	if current_user && current_user != @user
		# 		if  @reviews_received.find_by(reviewer_id: current_user.id) if current_user
		@gigs = @user.gigs_listing.where(active: true).paginate(page: params[:page], per_page: 10)
	end

	def delete_account
		@user = User.friendly.find(params[:user_id])
		if current_user != @user
			redirect_to root_url
		end
	end

	def destroy
		@user = User.friendly.find(params[:id])
		if current_user == @user
			@user.destroy
		else
			redirect_to root_url
		end
		redirect_to root_url
	end
end