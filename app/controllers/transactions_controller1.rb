class TransactionsController1 < ApplicationController
  
  # definitely want to add back in before prod
  skip_before_action :authenticate_user!,
    only: [:new, :create]

  #before_action :authenticate_user!

  def new
    @offer = Offer.friendly.find(params[:format])
    #@offer = Offer.friendly.find(params[:offer_id])
  end

  def pickup
    @sale = Sale.find_by!(guid: params[:guid])
    @offer = @sale.offer
  end

  def create
    @offer = Offer.friendly.find_by!(params[:id])
    puts "in transactions#create #{@offer.inspect}"

    token = params[:stripeToken]

    sale = Sale.new do |s|
      puts "offer price: #{@offer.price.inspect}"
      s.amount = @offer.price * 100,
      s.offer_id = @offer.id,
      s.stripe_token = token,
      s.email = params[:email]
    end
    sale.amount = @offer.price * 100
    puts "before sale.save, sale: #{sale.inspect}"

    if sale.save
      puts "in if sale.save #{sale.inspect}"
      StripeCharger.perform_async(sale.guid)
      render json: { guid: sale.guid }
    else
      errors = sale.errors.full_messages
      render json: {
        error: errors.join(" ")
      }, status: 400
    end

    # if sale.finished?
    #   redirect_to gig_path(@offer.gig), notice: "Success!"
    # else
    #   flash.now[:alert] = sale.error
    #   render :new
    # end
  end

  def status
    sale = Sale.find_by!(guid: params[:guid])

    render json: { status: sale.state }
  end
end














    # sale = @offer.create_sale(
    #   amount:       @offer.price * 100,
    #   email:        params[:email],
    #   stripe_token: params[:stripeToken]
    # )
    # sale.process!
    # if sale.finished?
    #   redirect_to gig_path(@offer.gig), notice: "Success!"
    # else
    #   flash.now[:alert] = sale.error
    #   render :new
    # end

    # token = params[:stripeToken]

    # begin
    #   charge = Stripe::Charge.create(
    #     amount:      offer.price*100, # important to * by 100
    #     currency:    "usd",
    #     source:      token,
    #     description: params[:stripeEmail]
    #   )
    #   @sale = offer.create_sale!(
    #     email:      params[:stripeEmail],
    #     stripe_id:  charge.id
    #   )
    #   flash[:notice] = "Success!"
    #   redirect_to gig_path(@sale.offer.gig)
    # rescue Stripe::CardError => e
    #   # The card has been declined or
    #   # some other error has occurred
    #   @error = e
    #   render :new
    # end
  # end


#   def download
#     @sale = Sale.find_by!(guid: params[:guid])

#     resp = HTTParty.get(@sale.product.file.url)

#     filename = @sale.product.file.url
#     send_data resp.body,
#       :filename => File.basename(filename),
#       :content_type => resp.headers['Content-Type']
#   end

