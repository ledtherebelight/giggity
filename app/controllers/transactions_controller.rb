class TransactionsController < ApplicationController
    before_action :authenticate_user!
    before_action :authenticate_user_phone!
    before_action :check_offer_not_purchased!

    def new
      @offer = Offer.friendly.find(params[:offer_id])
      gon.client_token = generate_client_token
    end

    def create
      @offer = Offer.friendly.find(params[:offer_id])
      unless current_user.has_payment_info?
        @result = Braintree::Transaction.sale(
          merchant_account_id: @offer.bidder.merchant_id,
          amount: @offer.price,
          service_fee_amount: (@offer.price * 0.1),
          payment_method_nonce: params[:payment_method_nonce],
          customer: {
            first_name: current_user.first_name,
            last_name: current_user.last_name,
            email: current_user.email
          },
        options: {
          submit_for_settlement: true,
          store_in_vault: true
        })
      else
        @result = Braintree::Transaction.sale(
          merchant_account_id: @offer.bidder.merchant_id,
          amount: @offer.price,
          service_fee_amount: (@offer.price * 0.1),
          payment_method_nonce: params[:payment_method_nonce],
          options: {
            submit_for_settlement: true
          })
      end

      if @result.success?
        current_user.update(braintree_customer_id: @result.transaction.customer_details.id) unless current_user.has_payment_info?
        @offer.purchase
        send_message_from_admin(@offer.bidder.id, "#{@offer.lister.first_name} just paid for your work! We'll deposit $#{@offer.price * 0.9} into your Venmo account.")
        redirect_to dashboard_path, notice: "Thanks! Your transaction was successful!"
      else
        flash[:warning] = "Something went wrong while processing your transaction. Please try again!"
        gon.client_token = generate_client_token
        render :new
      end
    end

    private

      def check_offer_not_purchased!
        @offer = Offer.friendly.find(params[:offer_id])
        if @offer.purchased == true
          flash[:warning] = "That offer was already purchased"
          redirect_to dashboard_path
        end
      end

      def generate_client_token
        if current_user.has_payment_info?
          Braintree::ClientToken.generate(customer_id: current_user.braintree_customer_id)
        else
          Braintree::ClientToken.generate
        end
      end
end