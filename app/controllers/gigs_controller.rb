class GigsController < ApplicationController
  before_action :set_gig, only: [:show, :edit, :update]
  before_action :authenticate_user!, except: [:show]
  before_action :authenticate_user_phone!, except: [:show]

  def index

  end

  def show
    @photos = @gig.photos

    # need to add later when reservations are correct
    #@booked = Offer.where("gig_id = ? AND user_id = ?",
    # @gig.id, current_user.id).present? if current_user

    @reviews = @gig.lister.reviews_received
    # Boolean to see if user has left a review
    #@hasReview = @reviews.find_by(reviewer_id: current_user.id) if current_user
    @gigsNearBy = Gig.where(active: true).near(@gig, 10, order: 'distance')
  end

  def new
    #@gig = current_user.gigs_listing.build
    @gig_category = GigCategory.friendly.find(params[:gig_category_id])

    #if params[:gig_sub_category_id]
    @gig_sub_category = GigSubCategory.friendly.find(params[:gig_sub_category_id])
    # elsif params[:gig][:gig_sub_category_id]
    #   @gig_sub_category = GigSubCategory.friendly.find(params[:gig][:gig_sub_category_id])
    #end

    #(flash[:gig]) ? flash[:gig] :
    @gig =  current_user.gigs_listing.build
  end

  def create
    @gig = current_user.gigs_listing.build(gig_params)

    if @gig.save

      if params[:images]
        params[:images].each do |image|
          @gig.photos.create(image: image)
        end
      end

      # if session[:gig]
      #   session.delete(:gig)
      # end

      @photos = @gig.photos
      redirect_to @gig, notice: "Gig submitted!"
    else
      @gig_category = GigCategory.friendly.find(params[:gig_category_id])
      @gig_sub_category = GigSubCategory.friendly.find(params[:gig_sub_category_id])
      flash[:gig] = @gig.id
      render :new
    end
  end

  def edit
    @gig = Gig.friendly.find(params[:id])
    @gig_category = @gig.gig_category
    @gig_sub_category = @gig.gig_sub_category
    if current_user.id == @gig.lister.id
      @photos = @gig.photos
    else
      redirect_to root_path, notice: "You don't have permission to edit that gig."
    end
  end

  def update
    @gig = Gig.friendly.find(params[:id])
    if @gig.update_attributes(gig_params)

      if params[:images]
        params[:images].each do |image|
          @gig.photos.create(image: image)
        end
      end

      redirect_to gig_path(@gig), notice: "Updated!"
    else
      flash[:warning] = "Something went wrong"
      @gig_category = @gig.gig_category
      @gig_sub_category = @gig.gig_sub_category
      render :edit
    end
  end

  def destroy
    current_user.gigs_listing.friendly.find(params[:id]).destroy
    #gig.cancel
    flash[:notice] = "Gig cancelled"
    redirect_to root_path
  end

  private
    def set_gig
      @gig = Gig.friendly.find(params[:id])
    end

    def gig_params
      params.require(:gig).permit(:gig_category_id, :gig_sub_category_id,
      :name, :summary, :lawn_mowing_time,
      :lawn_size, :bring_lawnmower, :bring_bags, :bring_weed_whacker,
      :bring_hedge_trimmer, :price, :address, :zip_code, :latitude, :longitude,
      :user_id, :active)
    end
end
