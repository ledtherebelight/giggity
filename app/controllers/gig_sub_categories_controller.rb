class GigSubCategoriesController < ApplicationController


    def show
        @gig_sub_category = GigSubCategory.friendly.find(params[:id])
    end

    def index
        @gig_category = GigCategory.friendly.find(params[:gig_category_id])
        @gig_sub_categories = GigSubCategory.where(gig_category_id: GigCategory.friendly.find(params[:gig_category_id]))
    end
end
