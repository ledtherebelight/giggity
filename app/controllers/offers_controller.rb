class OffersController < ApplicationController
	before_action :authenticate_user!, except: [:notify]
	before_action :authenticate_user_phone!, except: [:notify]

	# def preload
	# 	gig = Gig.friendly.find(params[:gig_id])
	# 	#today = Date.today
	# 	offers = gig.offers #.where("start_date >= ? OR end_date >= ?", today, today)

	# 	#render json: offers
	# end

	# def preview
	# 	start_date = Date.parse(params[:start_date])
	# 	end_date = Date.parse(params[:end_date])

	# 	output = {
	# 		conflict: is_conflict(start_date, end_date)
	# 	}

	# 	#render json: output
	# end

	def create
		@gig = Gig.friendly.find(params[:gig_id])
		@offer = current_user.offers_made.create(offer_params)

		if @offer.save
			redirect_to @gig, notice: "Offer submitted"
			send_message_from_admin(@offer.lister.id, "#{@offer.bidder.first_name} just offered to do your gig for $#{@offer.price}!")
		else
			flash[:warning] = "Offers must be whole numbers (minimum $5)"
			redirect_to @gig
		end
	end

	def edit
		@offer = Offer.friendly.find(params[:id])
		if !current_user.id == @offer.bidder.id
			redirect_to root_path, notice: "You don't have permission to edit that offer."
		end
		if @offer
			respond_to do |format|
				# need to do format.html
				format.js
			end
		end
	end

	def update
		@offer = Offer.friendly.find(params[:id])
		@gig = @offer.gig
		if !@offer.update(offer_params)
			flash.now[:notice] = "Your offer couldn't be updated :("
			render :edit
		end
	end

	def destroy
		@offer = Offer.friendly.find(params[:id])
		if current_user && @offer.bidder == current_user
			@offer.destroy
		end
		session[:return_to] ||= request.referer
		redirect_to session.delete(:return_to)
	end

	def inactivate
		@offer = current_user.offers_made.friendly.find(params[:id])
		@offer.inactivate
		@offer.save

		respond_to do |format|
			format.html
			format.js
		end
	end


	def accept
		offer = current_user.offers_received.friendly.find(params[:id])
		offer.accept
		offer.gig.active = false
		offer.save
		other_offers = offer.gig.offers.where(accepted: nil)
		other_offers.each do |offer|
			offer.reject
			offer.save
		end
		redirect_to dashboard_path
		#params[:price] = offer.price
		# redirect_to new_offer_transaction_path(offer)
	end

	def reject
		offer = current_user.offers_received.friendly.find(params[:id])
		offer.reject
		redirect_to dashboard_path
	end

	def sign_off
		offer = Offer.friendly.find(params[:id])
		if current_user == offer.lister
			offer.lister_sign_off
			# add later when payments implemented
			send_message_from_admin(offer.bidder.id, "#{offer.lister.first_name} signed off on your work for the '#{offer.gig.name}' gig.")
			# send_message_from_admin(offer.bidder.id, "They'll be redirected to a payment screen and we'll shoot you a message when the payment goes through.")
			flash[:notice] = "Signed off on the work."
			redirect_to new_offer_transaction_path(offer.id)
		elsif current_user == offer.bidder
			offer.bidder_sign_off
			if !offer.lister_signed_off?
				# add later when payments implemented
				send_message_from_admin(offer.lister.id, "#{offer.bidder.first_name} just signed off for your '#{offer.gig.name}' gig.")
			end
			flash[:notice] = "Signed off on the work."
			redirect_to dashboard_path
		else
			flash[:alert] = "Are you sure that's your gig?"
			redirect_to root_url
		end
	end

	# protect_from_forgery except: [:notify]
	# def notify
	# 	params.permit!
	# 	status = params[:payment_status]

	# 	offer = Offer.friendly.find(params[:item_number])

	# 	if status == "Completed"
	# 		offer.update_attributes status: true
	# 	else
	# 		offer.destroy
	# 	end

	# 	render nothing: true
	# end

	# protect_from_forgery except: [:your_trips]
	# def your_trips
	# 	@trips = current_user.offers.where("status = ?", true)
	# end

	def dashboard
	  @offers_made = current_user.offers_made.where(active: true).paginate(page: params[:page], per_page: 10)
	  @offers_received = current_user.offers_received.paginate(page: params[:page], per_page: 10)
		@gigs = current_user.gigs_listing.order("created_at desc").paginate(page: params[:page], per_page: 10)

		@reviews_made = Review.where(reviewer: current_user)

		respond_to do |format|
			format.html
			format.js
		end
	end

	def your_purchases
		@offers = current_user.offers_received.where(purchased: true)
	end

	private

		def is_conflict(start_date, end_date)
			gig = Gig.find(params[:gig_id])

			check = gig.offers.where("? < start_date AND end_date < ?", start_date, end_date)
			check.size > 0? true : false
		end

		def offer_params
			params.require(:offer).permit(:start_date, :end_date, :price, :gig_id)
		end
end