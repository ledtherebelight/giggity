class WebhooksController < ApplicationController
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }

  # def create
  #   sample_notification = Braintree::WebhookTesting.sample_notification(
  #     Braintree::WebhookNotification::Kind::SubscriptionWentPastDue,
  #     rand(10000)
  #   )

  #   signature = sample_notification[:bt_signature]
  #   payload = sample_notification[:bt_payload]
  #   NotificationStore.add(Braintree::WebhookNotification.parse(signature, payload))
  #   redirect_to webhooks_path
  # end

  # def destroy_all
  #   NotificationStore.clear
  #   redirect_to webhooks_path
  # end

  def handle
    webhook_notification = Braintree::WebhookNotification.parse(
      request.params["bt_signature"],
      request.params["bt_payload"]
    )
    puts "[Webhook Received #{webhook_notification.timestamp}] Kind: #{webhook_notification.kind}"

    # Merchant successful
    if webhook_notification.kind == Braintree::WebhookNotification::Kind::SubMerchantAccountApproved
      user = User.find_by(merchant_id: webhook_notification.merchant_account.id)
      user.update(merchant_account_approved: true)
      send_message_from_admin(user.id, "Congratulations! Your payment information was approved. You're set up to receive money into your account.")
    # Merchant failure
    elsif webhook_notification.kind == Braintree::WebhookNotification::Kind::SubMerchantAccountDeclined
      user = User.find_by(merchant_id: webhook_notification.merchant_account.id)
      send_message_from_admin(user.id, "Hmm... it looks like something went wrong with your account verification. Here's the message we got from Venmo.")
      send_message_from_admin(user.id, "'#{webhook_notification.message}'")
    end

    render nothing: true
  end

  # def index
  #   @notifications = NotificationStore.notifications
  # end

  def verify
    render :text => Braintree::WebhookNotification.verify(params[:bt_challenge])
  end
end
