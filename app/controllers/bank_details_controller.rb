class BankDetailsController < ApplicationController


  # before_filter :set_recipient

 def show

 end

  def create


    token = Stripe::Token.create(
      :bank_account => {
      :country => "US",
      :routing_number => params['routing_number'],
      :account_number => params['account_number']
      }
    )
    puts "token: #{token.inspect}"

    recipient = Stripe::Recipient.create(
      :name => current_user.name,
      :email => current_user.email,
      :type => "individual",
      :bank_account => token.id
    )
    puts "recipient: #{recipient.inspect}"

    current_user.recipient_id = recipient.id
    current_user.save
    puts "current user: #{current_user.inspect}"

    response_test = Stripe::Transfer.create(
      :amount => 1,
      :currency => "usd",
      :recipient => current_user.recipient_id,
      :description => "Example of money transfer"
    )

    puts "response: #{response_test.inspect}"

    flash[:notice] = "Successfully updated banking information"
    redirect_to user_path(current_user)
  end


  private

  # def set_recipient
  #   @recipient = current_user.stripe_recipient
  #   if @recipient.present?
  #     @recipient
  #   else
  #     @recipient = Stripe::Recipient.create(
  #         :name => current_user.name,
  #         :type => "individual",
  #         :email => current_user.email,
  #         :card => params[:stripe_token]
  #       )
  #       puts "recipient.inspect #{@recipient.inspect}"
  #       current_user.stripe_recipient_id = @recipient.id
  #       current_user.save
  #   end
  # end

   def update_bank_account
     @recipient.bank_account = params.slice(:account_number, :routing_number).merge(:country => 'US')
     @recipient.save
   end

   def update_recipient
     if @recipient.nil?
       @recipient = Stripe::Recipient.create(
         params.slice(:name, :tax_id).merge(:type => 'individual')
       )
       current_user.stripe_recipient_id = @recipient.id
       current_user.save!
     else
       #@recipient.tax_id = params[:tax_id]
       @recipient.name = current_user.name
       @recipient.save
     end
   end

end
