class OmniauthCallbacksController < Devise::OmniauthCallbacksController

	def facebook
		@user = User.from_omniauth(request.env["omniauth.auth"])

		if @user.persisted?
			sign_in_and_redirect @user, :event => :authentication
			set_flash_message(:notice, :success, :kind => "Facebook") if is_navigational_format?
		else
			session["devise.facebook_data"] = request.env["omniauth.auth"]
			redirect_to new_user_registration_url
		end
	end

	def google_oauth2

      @user = User.from_omniauth(request.env["omniauth.auth"])

      if @user.persisted?
        flash[:notice] = I18n.t "devise.omniauth_callbacks.success", :kind => "Google"
        sign_in_and_redirect @user, :event => :authentication
      else
        session["devise.google_data"] = request.env["omniauth.auth"]
        redirect_to new_user_registration_url
      end
  	end

  	# def stripe_connect
  	# 	@user = current_user
  	# 	if @user.update_attributes({
	  # 			access_code: request.env["omniauth.auth"].credentials.token,
	  # 			publishable_key: request.env["omniauth.auth"].info.stripe_publishable_key
	  # 		})
	  # 		sign_in_and_redirect @user, event: :authentication
	  # 		set_flash_message(:notice, :success, kind: "Stripe") if is_navigational_format?
  	# 	else
  	# 		session["devise.stripe_connect_data"] = request.env["omniauth.auth"]
  	# 		redirect_to new_user_registration_url
  	# 	end
  	# end
end