<%# app/views/gigs/index.js.erb %>
<% js = escape_javascript(
  render(partial: 'gigs/list', locals: { gigs: @gigs })
) %>