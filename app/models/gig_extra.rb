class GigExtra < ActiveRecord::Base
    belongs_to :gig
    
    enum lawn_size: [:small, :medium, :large]
    enum lawn_mowing_time: [:under_one_hour, :one_to_two_hours, :two_hours_or_more, :not_sure]
end
