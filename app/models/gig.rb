class Gig < ActiveRecord::Base

  #################################################
  # Friendly slug
  # makes a name like i-need-someone-to-mow-my-lawn-8vxh
  extend FriendlyId
  def unique_name
    "#{self.name.truncate_words(12).split(" ").join("-")}-#{SecureRandom.random_number(1_000).to_s(36)}"
  end
  friendly_id :unique_name, use: [:slugged, :history]

  #################################################
  # Associations

  # Lister Association
  belongs_to :lister, class_name: "User", foreign_key: :user_id

  # Bidder Association
  has_many :bidders, class_name: "User", through: :offers

  # Other Associations
  belongs_to :gig_category
  belongs_to :gig_sub_category
  has_many :offers, dependent: :destroy
  has_many :photos, dependent: :destroy

  #################################################
  # Validations
  validates :gig_category_id, presence: true, numericality: {only_integer: true}
  validates :gig_sub_category_id, presence: true, numericality: {only_integer: true}
  validates :name, presence: true, length: {maximum: 64}
  validates :summary, presence: true, length: {maximum: 3000}
  validates :address, presence: true
  validates_format_of :zip_code,
                      with: /\A\d{5}(-\d{4})?\z/i,
                      message: "should look like 12345"

  #################################################
  # Geocoding
  def address_full
    [address, zip_code].compact.join(', ')
  end

  geocoded_by :address_full
  reverse_geocoded_by :latitude, :longitude do |obj, results|
    if geo = results.first
      # populate your model
      obj.city    = geo.city
      obj.state   = geo.state
      obj.country = geo.country_code
    end
  end

  after_validation :geocode, :reverse_geocode, if: :address_changed?

  #################################################
  # Gig variables

  def self.lawn_sizes
    ['Small', 'Medium', 'Large']
  end

  def self.lawn_mowing_times
    ['Under 1 Hour', '1 to 2 Hours', '2 Hours or More', 'Not Sure']
  end

  # def cancel
  #   self.active = false
  #   save
  # end

end
