class Offer < ActiveRecord::Base

  extend FriendlyId
  friendly_id :offer_info, use: [:slugged, :history]

  belongs_to :gig
  belongs_to :bidder, class_name: "User", foreign_key: :user_id
  has_one :lister, class_name: "User", through: :gig
  has_one :sale
  has_many :reviews

  validates :price, presence: true, numericality:
    { only_integer: true, greater_than_or_equal_to: 5 }

  # Ensure unique offer between bidder / gig
  validates_uniqueness_of :user_id, scope: :gig_id

  scope :outstanding, -> { where(accepted: nil) }
  scope :accepted, -> { where(accepted: true) }
  scope :declined, -> { where(accepted: false) }

  def offer_info
    "#{self.gig.name.truncate_words(5).split(" ").join("-")}-with-#{self.bidder.first_name}-#{self.price}-#{SecureRandom.random_number(1_000_000).to_s(36)}"
  end

  def accept
    self.accepted = true
    save
  end

  def inactivate
    self.active = false
  end

  def reject
    self.accepted = false
    save
  end

  def purchase
    self.purchased = true
    save
  end

  def bidder_sign_off
    self.bidder_signed_off = true
    save
  end

  def lister_sign_off
    self.lister_signed_off = true
    save
  end
end
