class GigSubCategory < ActiveRecord::Base
    extend FriendlyId
    friendly_id :name, use: [:slugged, :history]

    belongs_to :gig_category
    has_many :gigs
end
