class User < ActiveRecord::Base
  before_save :capitalize_name
  after_create :send_welcome_message
  #after_update :update_merchant_info

  extend FriendlyId

  # makes names like "zach-the-ultra-mera"
  def superhero_name
    superhero_name = "#{self.first_name}-the-" + (Faker::Superhero.name).split(" ").join("-")
  end

  friendly_id :superhero_name, use: [:slugged, :history]

  validates_uniqueness_of :slug, message: "Someone is already using that name."

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :omniauthable

  acts_as_reader

  # def self.reader_scope
  #   where(Conversation.involving(self))
  # end

  #validates :superhero_name, uniqueness: true

  validates :first_name, presence: true, length: {maximum: 50}
  validates_format_of :first_name, with: /\A[a-zA-Z]+\z/, message: "letters only"
  validates :last_name, presence: true, length: {maximum: 50}
  validates_format_of :last_name, with: /\A[a-zA-Z]+\z/, message: "letters only"

  validates_inclusion_of :birthday,
    :in => Date.new(1900)..Time.now.years_ago(18).to_date,
    :message => 'Sorry. To sign up, you must be 18 or older.'

  validates :tos_accepted, acceptance: { accept: true, message: "Must be accepted" }

  # presence: true,
  #validates :zip_code, length: {maximum: 9}, numericality: {only_integer: true, greater_than_or_equal_to: 0}
  #validates_format_of :zip_code, :with => /\A\d{5}(-\d{4})?\z/, :message => "should look like 12345"

  has_one :phone_number, dependent: :destroy

  # Lister Associations
  has_many :gigs_listing, class_name: "Gig", dependent: :destroy
  has_many :offers_received, class_name: "Offer", through: :gigs_listing, source: :offers, dependent: :destroy

  # Bidder Associations
  has_many :offers_made, class_name: "Offer", dependent: :destroy
  has_many :gigs_bidding, class_name: "Gig", through: :offers_made, source: :gig, dependent: :destroy

  # Review Associations
  has_many :reviews_made, class_name: "Review", foreign_key: :reviewer_id
  has_many :reviews_received, class_name: "Review", foreign_key: :reviewed_id, dependent: :destroy

  # Avatar with paperclip
  has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100#" }, :default_url => "/images/:style/missing.png"
  #validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_attachment :avatar, content_type: { content_type: ["image/jpeg", "image/jpg", "image/png"] }, size: { in: 0..4.megabytes }

  after_validation :clean_paperclip_errors

  def clean_paperclip_errors
    errors.delete(:avatar)
  end

  # def stripe_recipient
  #   return nil if stripe_recipient_id.nil?
  #   puts "in stripe_recipient #{self.inspect}"
  #   Stripe::Recipient.retrieve stripe_recipient_id
  # end

  # geocoded_by :zip_code
  # after_validation :geocode, :reverse_geocode, if: :zip_code_changed?
  geocoded_by :current_sign_in_ip,
    latitude: :lat, longitude: :lon
  after_validation :geocode, :reverse_geocode, if: :current_sign_in_ip_changed?
  reverse_geocoded_by :latitude, :longitude do |obj, results|
    if geo = results.first
      # populate your model
      obj.city    = geo.city
      obj.state   = geo.state
      obj.country = geo.country_code
    end
  end

  def self.from_omniauth(auth)
    user = User.where(email: auth.info.email).first

    if user
      return user
    else
    	where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
        user.full_name = auth.info.name
        user.first_name = user.full_name.split(" ")[0].blank? ? "" : user.full_name.split(" ")[0]
        user.last_name = user.full_name.split(" ")[1].blank? ? "" : user.full_name.split(" ")[1]
        user.birthday = auth.extra.raw_info.birthday
        user.gender = auth.extra.raw_info.gender
        user.city = auth.extra.raw_info.location
        user.provider = auth.provider
        user.uid = auth.uid
        user.email = auth.info.email
        user.image = auth.info.image
        user.password = Devise.friendly_token[0,20]
      end
    end
  end

  def average_rating
    #bidder = bidder_reviews_received.count == 0 ? 0 : bidder_reviews_received.average(:star).round(2)
    #lister = lister_reviews_received.count == 0 ? 0 : lister_reviews_received.average(:star).round(2)
    #(bidder + lister) / 2
    reviews_received.count == 0 ? 0 : reviews_received.average(:star).round(2)
  end


  def has_payment_info?
    braintree_customer_id
  end

  def is_merchant?
    merchant_id
  end

  def name
    "#{first_name} #{last_name}"
  end

  def capitalize_name
    self.first_name = self.first_name.capitalize
    self.last_name = self.last_name.capitalize
  end

  private
    def send_welcome_message
      if User.find_by(email: "admin@giggity.rocks")
        if Conversation.between(User.find_by(email: "admin@giggity.rocks").id, self.id).present?
    			conversation = Conversation.between(User.find_by(email: "admin@giggity.rocks").id, self.id).first
    		else
    		  conversation_params = { sender_id: User.find_by(email: "admin@giggity.rocks").id, recipient_id: self.id }
    		  conversation = Conversation.create(conversation_params)
    		end
        message_params = { content: "Welcome to giggity.rocks! We're glad you're here. If you have any questions you can message us right here or with the green contact form in the bottom right. :)", user_id: User.find_by(email: "admin@giggity.rocks").id }
    		message = conversation.messages.create(message_params)
    	end
    end

    def update_merchant_info
      result = Braintree::MerchantAccount.update(
        "#{self.merchant_id}",
        :individual => {
          :first_name => self.first_name,
          :last_name => self.last_name,
          :email => self.email,
          :date_of_birth => "1981-11-19",
          :address => {
            # :street_address => params[:street_address],
            # :locality => params[:locality],
            # :region => params[:region],
            # :postal_code => params[:postal_code]
          }
        })
      if result.success?
        p "Merchant account successfully updated"
      else
        p result.errors
      end
    end

end
