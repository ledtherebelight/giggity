class PhoneNumber < ActiveRecord::Base
  belongs_to :user
  #validates_uniqueness_of :phone_number, message: "That phone number already belongs to an existing user."

  phony_normalize :phone_number, default_country_code: 'US'
  validates :phone_number, phony_plausible: true, presence: true, uniqueness: true

  def generate_pin
    #self.pin = rand(0000..9999).to_s.rjust(4, "0")
    var = rand(1..2)
    self.pin = var == 1 ? Faker::Team.creature.downcase : Faker::StarWars.character.split[0].downcase
    save
  end

  def twilio_client
    Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])
  end

  def send_pin
    twilio_client.messages.create(
      to: phone_number,
      from: ENV['TWILIO_PHONE_NUMBER'],
      body: "Your code is '#{pin}'"
    )
  end

  def verify(entered_pin, current_user)
    if self.pin == entered_pin
      update(verified: true)
      update(user: current_user)
      current_user.update_attribute(:phone_confirmation, true)
    else
      self.attempts += 1
    end
    save
  end

end
