class Review < ActiveRecord::Base
  belongs_to :reviewer, class_name: "User"
  belongs_to :reviewed, class_name: "User"
  belongs_to :offer

  # comment out for seeding database to allow admin reviews
  # validates_uniqueness_of :reviewed_id, scope: :offer_id
  # validates_uniqueness_of :reviewer_id, scope: :offer_id
end
