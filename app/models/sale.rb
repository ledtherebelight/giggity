class Sale < ActiveRecord::Base
  has_paper_trail
  #after_commit :charge_card

  include AASM

  aasm column: 'state' do
    state :pending, initial: true
    state :processing
    state :finished
    state :errored

    event :process, after: :charge_card do
      transitions from: :pending, to: :processing
    end

    event :finish do
      transitions from: :processing, to: :finished
    end

    event :fail do
      transitions from: :processing, to: :errored
    end
  end
  belongs_to :offer

  before_create :populate_guid
  validates_uniqueness_of :guid


  def charge_card

    puts "inside charge card method: #{self.inspect}"
    begin
      #save!
      puts "after begin: #{self.inspect}"
      charge = Stripe::Charge.create(
        {
        amount: self.amount,
        application_fee: self.amount/10,
        currency: "usd",
        source: self.stripe_token,
        description: self.email
        }
        #user.stripe_access_key # not sure about this
      )
      balance = Stripe::BalanceTransaction.retrieve(charge.balance_transaction)
      self.update(
        stripe_id:       charge.id,
        card_expiration: Date.new(charge.source.exp_year, charge.source.exp_month, 1),
        fee_amount:      balance.fee
      )
      self.finish!
    rescue Stripe::StripeError => e
      self.update_attributes(error: e.message)
      self.fail!
    end
  end

  private

    def populate_guid
      if new_record?
          self.guid = SecureRandom.uuid()
      end
    end


end
