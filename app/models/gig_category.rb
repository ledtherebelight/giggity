class GigCategory < ActiveRecord::Base
    extend FriendlyId
    friendly_id :name, use: [:slugged, :history]
    
    has_many :gig_sub_categories
    has_many :gigs

    validates :name, presence: true
end




   #has_many :gigs, through: :gig_categories
    # belongs_to :parent, class_name: "Category", foreign_key: :parent_id
    # has_many :children, class_name: "Category", foreign_key: :parent_id
    # validates :name, uniqueness: { case_sensitive: false }

    # has_many :category_attributes