class Photo < ActiveRecord::Base
  belongs_to :gig

  has_attached_file :image,
    styles: { large: "600x600>", medium: "300x300>", thumb: "100x100>" },
    storage: :s3,
    s3_credentials: { access_key_id: ENV["AWS_KEY"], secret_access_key: ENV["AWS_SECRET"] },
    bucket: "giggity-rocks"

  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/

  validates_attachment :image, content_type: { content_type: ["image/jpeg", "image/jpg", "image/gif", "image/png"] },
  size: { in: 0..5.megabytes, message: "Image must be less than 5 megabytes" }

end
