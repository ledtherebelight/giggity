class ApplicationMailer < ActionMailer::Base
  default from: "Admin @ giggity"
  layout 'mailer'
end
