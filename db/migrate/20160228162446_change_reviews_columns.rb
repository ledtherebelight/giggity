class ChangeReviewsColumns < ActiveRecord::Migration
  def change

    rename_column :reviews, :gig_id, :reviewed_id
    rename_column :reviews, :user_id, :reviewer_id
  end
end
