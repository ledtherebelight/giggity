class ChangeListerBool < ActiveRecord::Migration
  def change
    rename_column :reviews, :lister, :is_for_lister
  end
end
