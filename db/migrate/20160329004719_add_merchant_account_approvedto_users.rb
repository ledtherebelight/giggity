class AddMerchantAccountApprovedtoUsers < ActiveRecord::Migration
  def change
    add_column :users, :merchant_account_approved, :boolean
  end
end
