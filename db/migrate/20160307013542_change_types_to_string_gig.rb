class ChangeTypesToStringGig < ActiveRecord::Migration
  def change
    change_column :gigs, :lawn_size, :string
    change_column :gigs, :lawn_mowing_time, :string
  end
end
