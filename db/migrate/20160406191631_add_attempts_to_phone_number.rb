class AddAttemptsToPhoneNumber < ActiveRecord::Migration
  def change
    add_column :phone_numbers, :attempts, :integer
  end
end
