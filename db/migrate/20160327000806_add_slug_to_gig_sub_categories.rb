class AddSlugToGigSubCategories < ActiveRecord::Migration
  def change
    add_column :gig_sub_categories, :slug, :string
  end
end
