class RemoveGigIdFromGigSubCategory < ActiveRecord::Migration
  def change
    remove_column :gig_sub_categories, :gig_id
  end
end
