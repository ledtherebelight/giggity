class CreateGigExtras < ActiveRecord::Migration
  def change
    create_table :gig_extras do |t|
      t.integer :gig_id, index: true
      t.integer :lawn_size, index: true
      t.integer :lawn_mowing_time, index: true
      t.boolean :bring_lawn_mower
      t.boolean :bring_bags
      t.boolean :bring_weed_whacker
      t.boolean :bring_hedge_trimmer


      t.boolean :haul_bags_away

      t.timestamps null: false
    end
  end
end
