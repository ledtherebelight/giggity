class CreateCategoryAttributes < ActiveRecord::Migration
  def change
    create_table :category_attributes do |t|

      t.timestamps null: false
    end
  end
end
