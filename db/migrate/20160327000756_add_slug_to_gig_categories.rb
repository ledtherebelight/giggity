class AddSlugToGigCategories < ActiveRecord::Migration
  def change
    add_column :gig_categories, :slug, :string
  end
end
