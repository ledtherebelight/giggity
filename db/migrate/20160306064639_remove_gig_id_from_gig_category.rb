class RemoveGigIdFromGigCategory < ActiveRecord::Migration
  def change
    remove_column :gig_categories, :gig_id
  end
end
