class AddDescriptionToGigSubCategory < ActiveRecord::Migration
  def change
    add_column :gig_sub_categories, :description, :string
  end
end
