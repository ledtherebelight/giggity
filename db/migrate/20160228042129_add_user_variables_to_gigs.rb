class AddUserVariablesToGigs < ActiveRecord::Migration
  def change
    add_column :gigs, :user_var1, :integer
    add_column :gigs, :user_var2, :integer
    add_column :gigs, :user_var3, :integer
    add_column :gigs, :user_var4, :string
    add_column :gigs, :user_var5, :string
  end
end
