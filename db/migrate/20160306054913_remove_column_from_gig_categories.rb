class RemoveColumnFromGigCategories < ActiveRecord::Migration
  def change
    remove_column :gig_categories, :category_id
  end
end
