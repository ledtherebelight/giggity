class AddSignOffToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :bidder_signed_off, :boolean
    add_column :offers, :lister_signed_off, :boolean
  end
end
