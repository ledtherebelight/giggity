class AddFieldsToCategoryAttributes < ActiveRecord::Migration
  def change
    add_column :category_attributes, :string1, :string
    add_column :category_attributes, :string2, :string
    add_column :category_attributes, :string3, :string
    add_column :category_attributes, :string4, :string
    add_column :category_attributes, :string5, :string

    add_column :category_attributes, :int1, :integer
    add_column :category_attributes, :int2, :integer
    add_column :category_attributes, :int3, :integer
    add_column :category_attributes, :int4, :integer
    add_column :category_attributes, :int5, :integer

    add_column :category_attributes, :bool1, :boolean
    add_column :category_attributes, :bool2, :boolean
    add_column :category_attributes, :bool3, :boolean
    add_column :category_attributes, :bool4, :boolean
    add_column :category_attributes, :bool5, :boolean
  end
end
