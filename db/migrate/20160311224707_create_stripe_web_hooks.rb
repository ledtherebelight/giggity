class CreateStripeWebHooks < ActiveRecord::Migration
  def change
    create_table :stripe_web_hooks do |t|
      t.string :stripe_id

      t.timestamps null: false
    end
  end
end
