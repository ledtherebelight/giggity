class CreateGigSubCategories < ActiveRecord::Migration
  def change
    create_table :gig_sub_categories do |t|
      t.timestamps null: false
      t.string :name
      t.integer :gig_category_id
    end
  end
end
