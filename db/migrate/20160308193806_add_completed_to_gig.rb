class AddCompletedToGig < ActiveRecord::Migration
  def change
    add_column :gigs, :completed, :boolean
  end
end
