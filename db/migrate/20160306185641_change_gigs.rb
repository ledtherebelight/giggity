class ChangeGigs < ActiveRecord::Migration
  def change
    remove_column :gigs, :home_type
    remove_column :gigs, :is_extra1
    remove_column :gigs, :is_extra2
    remove_column :gigs, :is_extra3
    remove_column :gigs, :is_extra4
    remove_column :gigs, :is_extra5
    remove_column :gigs, :is_extra6
    remove_column :gigs, :is_extra7
    remove_column :gigs, :user_var1
    remove_column :gigs, :user_var2
    remove_column :gigs, :user_var3
    remove_column :gigs, :user_var4
    remove_column :gigs, :user_var5
    rename_column :gigs, :gig_name, :name
  end
end
