class DeleteFieldsFromGigs < ActiveRecord::Migration
  def change
  	remove_column :gigs, :accommodate
  	remove_column :gigs, :bed_gig
  	remove_column :gigs, :bath_gig
  	remove_column :gigs, :is_internet
  	
  	add_column :gigs, :gig_category, :string
  	add_column :gigs, :zip_code, :integer
  end
end
