class CreateNotificationStores < ActiveRecord::Migration
  def change
    create_table :notification_stores do |t|

      t.timestamps null: false
    end
  end
end
