class AddExtrasToGigs < ActiveRecord::Migration
  def change
    add_column :gigs, :is_extra5, :boolean
    add_column :gigs, :is_extra6, :boolean
    add_column :gigs, :is_extra7, :boolean
  end
end
