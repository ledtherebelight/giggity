class AddCategoryFieldsToGig < ActiveRecord::Migration
  def change
    add_column :gigs, :lawn_size, :integer
    add_column :gigs, :lawn_mowing_time, :integer
    add_column :gigs, :bring_lawnmower, :boolean
    add_column :gigs, :bring_bags, :boolean
    add_column :gigs, :bring_weed_whacker, :boolean
    add_column :gigs, :bring_hedge_trimmer, :boolean
    add_column :gigs, :haul_leaves_away, :boolean
  end
end

