class AddTosAcceptedToUsers < ActiveRecord::Migration
  def change
    add_column :users, :tos_accepted, :boolean, default: false
  end
end
