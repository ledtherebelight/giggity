class AddPhoneConfirmationToUsers < ActiveRecord::Migration
  def change
    add_column :users, :phone_confirmation, :boolean, default: false
  end
end
