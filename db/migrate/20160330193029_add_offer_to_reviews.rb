class AddOfferToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :offer_id, :integer
    add_index :reviews, :offer_id
  end
end
