class AddCityStateCountryToGig < ActiveRecord::Migration
  def change

    add_column :gigs, :city, :string
    add_column :gigs, :state, :string
    add_column :gigs, :country, :string
  end
end
