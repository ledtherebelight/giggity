class ChangeGigsListingNameToGigName < ActiveRecord::Migration
  def change
    rename_column :gigs, :listing_name, :gig_name
  end
end
