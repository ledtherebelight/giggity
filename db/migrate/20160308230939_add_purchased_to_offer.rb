class AddPurchasedToOffer < ActiveRecord::Migration
  def change
    add_column :offers, :purchased, :boolean, default: false
  end
end
