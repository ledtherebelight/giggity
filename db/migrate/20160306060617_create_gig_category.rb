class CreateGigCategory < ActiveRecord::Migration
  def change
    create_table :gig_categories do |t|
      t.string :name
      t.integer :gig_id
    end
  end
end
