class AddIdsToGigs < ActiveRecord::Migration
  def change
    add_column :gigs, :gig_category_id, :integer
    add_column :gigs, :gig_sub_category_id, :integer
  end
end
