class FixColumnName < ActiveRecord::Migration
  def self.up
  	rename_column :gigs, :is_tv, :is_extra1
  	rename_column :gigs, :is_kitchen, :is_extra2
  	rename_column :gigs, :is_air, :is_extra3
  	rename_column :gigs, :is_heating, :is_extra4
  end

  def self.down
  	rename_column :gigs, :is_extra1, :is_tv
  	rename_column :gigs, :is_extra2, :is_kitchen
  	rename_column :gigs, :is_extra3, :is_air
  	rename_column :gigs, :is_extra4, :is_heating
  end
end
