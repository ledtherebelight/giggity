class AddGigIdToGigSubCategory < ActiveRecord::Migration
  def change
    add_column :gig_sub_categories, :gig_id, :integer
  end
end
