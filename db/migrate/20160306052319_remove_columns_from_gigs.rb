class RemoveColumnsFromGigs < ActiveRecord::Migration
  def change
    remove_column :gigs, :gig_category
    remove_column :gigs, :gig_type
  end
end
