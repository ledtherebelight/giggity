class ReorderGigsFields < ActiveRecord::Migration
  def change
  	change_table :gigs do |g|
  		g.change :gig_type, :string, after: :gig_category
  		g.change :listing_name, :string, after: :gig_type
  		g.change :summary, :text, after: :listing_name
  		g.change :price, :integer, after: :summary
  		g.change :home_type, :string, after: :price
  		g.change :address, :string, after: :home_type
  		g.change :zip_code, :integer, after: :address
  		g.change :latitude, :float, after: :zip_code
  		g.change :longitude, :float, after: :latitude
  		g.change :is_extra1, :boolean, after: :longitude
  		g.change :is_extra2, :boolean, after: :is_extra1
  		g.change :is_extra3, :boolean, after: :is_extra2
  		g.change :is_extra4, :boolean, after: :is_extra3
  		g.change :user_id, :integer, after: :is_extra4
  		g.change :active, :boolean, after: :user_id
  		g.change :created_at, :datetime, after: :active
  		g.change :updated_at, :datetime, after: :created_at
  	end
  end
end


