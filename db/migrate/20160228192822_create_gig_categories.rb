class CreateGigCategories < ActiveRecord::Migration
  def change
    create_table :gig_categories, id: false do |t|

      t.integer :category_id
      t.integer :gig_id
    end
  end
end
