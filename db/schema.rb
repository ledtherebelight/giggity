# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160413014440) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "categories", force: :cascade do |t|
    t.string  "name"
    t.integer "parent_id"
  end

  create_table "category_attributes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "string1"
    t.string   "string2"
    t.string   "string3"
    t.string   "string4"
    t.string   "string5"
    t.integer  "int1"
    t.integer  "int2"
    t.integer  "int3"
    t.integer  "int4"
    t.integer  "int5"
    t.boolean  "bool1"
    t.boolean  "bool2"
    t.boolean  "bool3"
    t.boolean  "bool4"
    t.boolean  "bool5"
  end

  create_table "category_attributes_tables", force: :cascade do |t|
  end

  create_table "conversations", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "gig_categories", force: :cascade do |t|
    t.string "name"
    t.string "slug"
  end

  create_table "gig_extras", force: :cascade do |t|
    t.integer  "gig_id"
    t.integer  "lawn_size"
    t.integer  "lawn_mowing_time"
    t.boolean  "bring_lawn_mower"
    t.boolean  "bring_bags"
    t.boolean  "bring_weed_whacker"
    t.boolean  "bring_hedge_trimmer"
    t.boolean  "haul_bags_away"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "gig_extras", ["gig_id"], name: "index_gig_extras_on_gig_id", using: :btree
  add_index "gig_extras", ["lawn_mowing_time"], name: "index_gig_extras_on_lawn_mowing_time", using: :btree
  add_index "gig_extras", ["lawn_size"], name: "index_gig_extras_on_lawn_size", using: :btree

  create_table "gig_sub_categories", force: :cascade do |t|
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "name"
    t.integer  "gig_category_id"
    t.string   "description"
    t.string   "slug"
  end

  create_table "gigs", force: :cascade do |t|
    t.string   "name"
    t.text     "summary"
    t.string   "address"
    t.integer  "price"
    t.boolean  "active"
    t.integer  "user_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "zip_code"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.integer  "gig_category_id"
    t.integer  "gig_sub_category_id"
    t.string   "lawn_size"
    t.string   "lawn_mowing_time"
    t.boolean  "bring_lawnmower"
    t.boolean  "bring_bags"
    t.boolean  "bring_weed_whacker"
    t.boolean  "bring_hedge_trimmer"
    t.boolean  "haul_leaves_away"
    t.boolean  "completed"
    t.string   "slug"
  end

  add_index "gigs", ["slug"], name: "index_gigs_on_slug", unique: true, using: :btree
  add_index "gigs", ["user_id"], name: "index_gigs_on_user_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.text     "content"
    t.integer  "conversation_id"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "messages", ["conversation_id"], name: "index_messages_on_conversation_id", using: :btree
  add_index "messages", ["created_at"], name: "index_messages_on_created_at", using: :btree
  add_index "messages", ["user_id"], name: "index_messages_on_user_id", using: :btree

  create_table "notification_stores", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "offers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "gig_id"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "price"
    t.integer  "total"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.boolean  "status"
    t.boolean  "accepted"
    t.boolean  "purchased",         default: false
    t.boolean  "bidder_signed_off"
    t.boolean  "lister_signed_off"
    t.string   "slug"
    t.string   "comment"
    t.boolean  "active",            default: true
  end

  add_index "offers", ["gig_id"], name: "index_offers_on_gig_id", using: :btree
  add_index "offers", ["slug"], name: "index_offers_on_slug", unique: true, using: :btree
  add_index "offers", ["user_id"], name: "index_offers_on_user_id", using: :btree

  create_table "phone_numbers", force: :cascade do |t|
    t.string   "phone_number"
    t.string   "pin"
    t.boolean  "verified"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "attempts"
    t.integer  "user_id"
  end

  add_index "phone_numbers", ["user_id"], name: "index_phone_numbers_on_user_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.integer  "gig_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "photos", ["gig_id"], name: "index_photos_on_gig_id", using: :btree

  create_table "read_marks", force: :cascade do |t|
    t.integer  "readable_id"
    t.string   "readable_type", null: false
    t.integer  "reader_id"
    t.string   "reader_type",   null: false
    t.datetime "timestamp"
  end

  add_index "read_marks", ["reader_id", "reader_type", "readable_type", "readable_id"], name: "read_marks_reader_readable_index", using: :btree

  create_table "reviews", force: :cascade do |t|
    t.text     "comment"
    t.integer  "star",          default: 1
    t.integer  "reviewed_id"
    t.integer  "reviewer_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "is_for_lister"
    t.integer  "offer_id"
  end

  add_index "reviews", ["offer_id"], name: "index_reviews_on_offer_id", using: :btree
  add_index "reviews", ["reviewed_id"], name: "index_reviews_on_reviewed_id", using: :btree
  add_index "reviews", ["reviewer_id"], name: "index_reviews_on_reviewer_id", using: :btree

  create_table "sales", force: :cascade do |t|
    t.string   "email"
    t.string   "guid"
    t.integer  "offer_id"
    t.string   "stripe_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "state"
    t.string   "stripe_token"
    t.date     "card_expiration"
    t.text     "error"
    t.integer  "fee_amount"
    t.integer  "amount"
  end

  add_index "sales", ["offer_id"], name: "index_sales_on_offer_id", using: :btree

  create_table "stripe_web_hooks", force: :cascade do |t|
    t.string   "stripe_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                     default: "",    null: false
    t.string   "encrypted_password",        default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",             default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "fullname"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "image"
    t.string   "phone"
    t.text     "description"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "full_name"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "zip_code"
    t.string   "gender"
    t.date     "birthday"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "age"
    t.string   "address"
    t.string   "publishable_key"
    t.string   "access_code"
    t.string   "stripe_id"
    t.string   "stripe_access_key"
    t.string   "stripe_publishable_key"
    t.string   "stripe_refresh_token"
    t.string   "slug"
    t.string   "recipient_id"
    t.string   "braintree_customer_id"
    t.string   "merchant_id"
    t.boolean  "merchant_account_approved"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.boolean  "phone_confirmation",        default: false
    t.string   "pin"
    t.boolean  "tos_accepted",              default: false
    t.boolean  "fake"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "gigs", "users"
  add_foreign_key "messages", "conversations"
  add_foreign_key "messages", "users"
  add_foreign_key "offers", "gigs"
  add_foreign_key "offers", "users"
  add_foreign_key "phone_numbers", "users"
  add_foreign_key "photos", "gigs"
  add_foreign_key "reviews", "gigs", column: "reviewed_id"
  add_foreign_key "reviews", "users", column: "reviewer_id"
  add_foreign_key "sales", "offers"
end
